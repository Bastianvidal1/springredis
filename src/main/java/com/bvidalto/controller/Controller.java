package com.bvidalto.controller;


import com.bvidalto.domain.Rol;
import com.bvidalto.domain.Student;
import com.bvidalto.repository.StudentRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class Controller {
    private StudentRepository studentRepository;

    public Controller(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping("/login")
    public String login(@RequestHeader(value = "emailAddress", defaultValue = "none")String emailAddress, @RequestHeader(value = "password" , defaultValue = "none")String password){

        //LOGICA DE CONSULTA EN SERVICIO DE CUENTAS Y ESCRITURA DE DATOS EN REDIS

        Student student = new Student();
        student.setEmailAddress(emailAddress);
        student.setPassword(password);

        Map<String, Student> map = studentRepository.findAll();

        for(Map.Entry<String, Student> item : map.entrySet()){

            if(item.getValue().getEmailAddress().equals(student.getEmailAddress()) && item.getValue().getPassword().equals(student.getPassword())){
                return  item.getKey();
            }

        }



        return "";
    }


    @GetMapping("/students")
    public Map<String, Student> findAll(){
        return studentRepository.findAll();
    }

    @GetMapping("/students/{id}")
    public Student findById(@PathVariable String id){
        return studentRepository.findById(id);
    }


    @PostMapping("/students")
    public void createStudent(@RequestBody Student student){
        studentRepository.save(student);
    }

    @DeleteMapping("/students/{id}")
    public void deleteStudent(@PathVariable String id){
        studentRepository.delete(id);
    }

    @GetMapping("/test")
    public String test(){
        return "Test services";
    }


    @GetMapping("/validateBusinessEnviroment/{id}")
    public boolean validarAmbienteEmpresa(@PathVariable String id){
        Student student = studentRepository.findById(id);

        for(Rol item : student.getRoles()){
            if(item.getName().equals("EMPRESA")){
                return true;
            }
        }


        return false;
    }






}
