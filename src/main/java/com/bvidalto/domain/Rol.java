package com.bvidalto.domain;

import java.io.Serializable;

public class Rol  implements Serializable {
    private String name;


    public Rol(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
